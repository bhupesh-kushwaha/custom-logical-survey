<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Survey;
use App\SurveyAnswer;

class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        $surveyList = Survey::orderBy('id', 'desc')->pluck('name', 'id');
        $selected = 0;
        $surveyViewBox = [];

        if( $request->get('action') === 'result' && $request->get('id') ) {

            $surveyResult = SurveyAnswer::where('survey_id',$request->get('id'))
            ->orderBy('question_id', 'asc')->get();

            $array = [];
            foreach($surveyResult as $item) {
                $arr = array();

                $arr['username'] = $item->users->name;

                $arr['created_at'] = $item->created_at->format('d M, Y');

                $arr['question'] = $item->questions->name;

                if ($item->is_option == 1):
                    $arr1 = explode(',', $item->answer);
                    $getData = \App\QuestionOption::whereIn('id', $arr1)->get()->pluck('name')->toArray();

                    $arr['answer'] = implode(',', $getData);
                else:
                    $arr['answer'] = $item->answer;
                endif;

                array_push( $array, $arr );
            }

            $groupedByUser = collect($array)->groupBy('username');

            $groupedByUser->toArray();

            $surveyViewBox[] = view('Components.Box', compact('groupedByUser'))->render();

            $selected = $request->get('id');
        }

        return view('admin.index', compact('surveyList', 'surveyViewBox', 'selected'));
    }
}
