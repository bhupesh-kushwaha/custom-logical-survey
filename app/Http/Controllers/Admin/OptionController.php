<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Traits\HelperTrait;
use App\Http\Requests\QuestionOptionRequest;
use App\Question;
use Illuminate\Support\Facades\Response;
use App\QuestionOption;

class OptionController extends Controller
{
    use HelperTrait;

   /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Question $question, $is_multiple)
    {
        $options = $question->options()->paginate(5);

        $questionId = $question->id;
        $questionName = $question->name;
        $surveyID = $question->surveies->id;

        return view('admin.option.index', compact('options', 'questionId', 'questionName', 'surveyID', 'is_multiple'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  QuestionOptionRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(QuestionOptionRequest $request)
    {
        $question = QuestionOption::updateOrCreate(
            [
                'id' => $request->option_id,
                'question_id' => $request->question_id
            ],
            [
                'name' => $request->option_name
            ]
        );

        return Response::json($question);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $question
     * @param  int  $question
     * @return \Illuminate\Http\Response
     */
    public function edit($question, $option)
    {
        $options  = QuestionOption::where(['id' => $option])->first();

        return Response::json($options);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $options = QuestionOption::where('id',$id)->delete();

        return Response::json($options);
    }
}

