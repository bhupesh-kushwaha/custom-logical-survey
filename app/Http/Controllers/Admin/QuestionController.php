<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Traits\HelperTrait;
use App\Http\Requests\QuestionRequest;
use App\Question;
use App\QuestionOption;
use App\QuestionType;
use App\Survey;
use Illuminate\Support\Facades\Response;

class QuestionController extends Controller
{
    use HelperTrait;

   /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Survey $survey)
    {
        $questions = $survey->questions()->where('question_id', 0)->paginate(5);

        $questionType = QuestionType::orderBy('name', 'desc')->pluck('name', 'id');

        $surveyId = $survey->id;
        $surveyName = $survey->name;

        return view('admin.question.index', compact('questions', 'questionType', 'surveyId', 'surveyName'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function nestedQuestion($question, $option)
    {
        $questions = Question::where([
            'question_id' => $question,
            'option_id' => $option,
        ])->paginate(5);

        $questionType = QuestionType::orderBy('name', 'desc')->pluck('name', 'id');

        $questionDetails = Question::where('id', $question)->first();

        $optionDetails = QuestionOption::where('id', $option)->first();

        return view('admin.question.option.index', compact('questions', 'questionType', 'questionDetails', 'optionDetails'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  QuestionRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(QuestionRequest $request)
    {
        if ($request->has(['parent_question_id', 'parent_option_id'])
            && $request->parent_question_id != 0 && $request->parent_option_id != 0
        ) {
            $question = Question::updateOrCreate(
                [
                    'id' => $request->question_id,
                    'survey_id' => $request->survey_id,
                    'question_id' => $request->parent_question_id,
                    'option_id' => $request->parent_option_id
                ],
                [
                    'question_type_id' => $request->question_type,
                    'name' => $request->question_name,
                    'mulitple' => ( !empty($request->question_multiple) ? $request->question_multiple : 0),
                ]
            );
        }
        else
        {
            $question = Question::updateOrCreate(
                [
                    'id' => $request->question_id,
                    'survey_id' => $request->survey_id
                ],
                [
                    'question_type_id' => $request->question_type,
                    'name' => $request->question_name,
                    'mulitple' => ( !empty($request->question_multiple) ? $request->question_multiple : 0),
                ]
            );
        }

        return Response::json($question);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $question
     * @param  int  $survey
     * @return \Illuminate\Http\Response
     */
    public function edit($survey, $question)
    {
        $questions  = Question::where(['id' => $question])->first();

        return Response::json($questions);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $questions = Question::where('id',$id)->delete();

        return Response::json($questions);
    }
}

