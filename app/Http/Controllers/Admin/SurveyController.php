<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\SurveyRequest;
use App\Survey;
use Illuminate\Support\Facades\Response;

class SurveyController extends Controller
{

   /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $surveies = Survey::orderBy('id','desc')->paginate(5);

        return view('admin.survey.index', compact('surveies'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  SurveyRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SurveyRequest $request)
    {
        $survey = Survey::updateOrCreate(
            ['id' => $request->survey_id],
            [
                'name' => $request->survey_name,
                'expiry_date' => $request->survey_expiry_date,
            ]
        );

        return Response::json($survey);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $survey
     * @return \Illuminate\Http\Response
     */
    public function edit($survey)
    {
        $survey  = Survey::where(['id' => $survey])->first();

        return Response::json($survey);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $survey = Survey::where('id',$id)->delete();

        return Response::json($survey);
    }
}

