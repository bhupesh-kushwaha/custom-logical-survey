<?php

namespace App\Http\Controllers\Api\V1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Interfaces\ApiInterfaces;
use App\Http\Traits\HelperTrait;
use App\User;
use App\Product;
use App\Category;
use App\Survey;

class ApiController extends Controller implements ApiInterfaces
{
    use HelperTrait;

    /**
     * The request implementation.
     *
     * @var request
     */
    protected $request;

    /**
     * Get all caltegory details.
     *
     * @param  Category  $category
     * @return mixed
     */
    public function __construct(Request $request) {
        $this->request = $request;
    }

    /**
     * Get all survey details.
     *
     * @return array
     */
    public function showSurvey() {
        $categoryData = Survey::orderBy('id', 'desc')->get();

        return $this->generateResponse(200, 'success', 'Survey data',
            $this->fetchResourceData('survey', $categoryData)
        );
    }
}
