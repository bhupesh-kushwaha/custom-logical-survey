<?php

namespace App\Http\Controllers;

use App\Survey;
use App\SurveyAnswer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $surveyList = Survey::orderBy('id', 'desc')->pluck('name', 'id');

        return view('home', compact('surveyList'));
    }

     /**
     * Show the form for editing the specified resource.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function getSurvey(Survey $survey)
    {

        $surveyRender = array();
        $url = route('ajax.questions');

        foreach ($survey->questions->where('question_id', 0) as $questions) {

            $surveyRender[] = view('Components.Field', compact('survey', 'questions', 'url'))->render();
        }

        return view('survey', compact('surveyRender', 'survey'));
    }

    /**
     * Store a newly created resource in storage. type_{surveyID}{questionID}
     *
     * @param  SurveyRequest  $request
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function saveSurvey(Request $request)
    {
        foreach ($request->except('_token', 'btnSubmit') as $key => $value) {

            $keyArray = explode('_', $key);

            if( is_array($keyArray) ) {

                $isOption = 0;

                if( $keyArray[0] == 'checkbox' ) {
                    $value = implode(",", $value);

                    $isOption = 1;
                }

                if( $keyArray[0] == 'radio' ) {
                    $isOption = 1;
                }

                if( !empty($value)) {
                    $answer =  new SurveyAnswer();
                    $answer->user_id = Auth::user()->id;
                    $answer->survey_id = $keyArray[1];
                    $answer->question_id = $keyArray[2];
                    $answer->answer = $value;
                    $answer->is_option = $isOption;
                    $answer->save();
                }
            }

            $surveyID = $keyArray[1];
        }

        return redirect()->route('home.survey', ['survey' => $surveyID])
        ->with('status', 'Survey successfully updated');
    }

    public function ajaxMultipleQuestions(Request $request)
    {
        $url = route('ajax.questions');

        $surveyRender = array();

        $survey = Survey::where('id', $request->survey_id)->first();

        foreach ($survey->questions
            ->where('question_id', $request->quesion_id)
            ->where('option_id', $request->option_id) as $questions
        ) {
            $surveyRender[] = view('Components.Field', compact('survey', 'questions', 'url'))->render();
        }


        return response()->json(array('success' => true, 'html'=>$surveyRender));
    }
}

