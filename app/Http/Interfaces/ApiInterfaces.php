<?php

namespace App\Http\Interfaces;

interface ApiInterfaces {

    /**
     * Get all survey details.
     *
     * @return array
     */
    public function showSurvey();
}
