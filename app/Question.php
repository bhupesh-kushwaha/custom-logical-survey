<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    const QUESTION_MULTIPLE_YES = 1;
    const QUESTION_MULTIPLE_NO = 0;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'survey_id', 'question_type_id', 'name', 'mulitple','question_id', 'option_id'
    ];

    /**
     * Get the survey that owns the questions.
     */
    public function surveies()
    {
        return $this->belongsTo(Survey::class, 'survey_id', 'id');
    }

    /**
     * Get the phone record associated with the user.
     */
    public function question_types()
    {
        return $this->hasMany(QuestionType::class, 'id', 'question_type_id');
    }

     /**
     * Get the options for the questions.
     */
    public function options()
    {
        return $this->hasMany(QuestionOption::class, 'question_id', 'id');
    }
}
