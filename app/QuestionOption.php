<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class QuestionOption extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'question_id', 'name'
    ];

    /**
     * Get the questions that owns the options.
     */
    public function questions()
    {
        return $this->belongsTo(Question::class, 'question_id', 'id');
    }
}
