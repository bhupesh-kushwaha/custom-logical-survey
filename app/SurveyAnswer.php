<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SurveyAnswer extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'survey_id', 'question_id', 'answer', 'is_option'
    ];

    /**
     * Get the answer for the user.
     */
    public function users()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }

    /**
     * Get the answer for the survey.
     */
    public function surveies()
    {
        return $this->hasOne(Survey::class, 'id', 'survey_id');
    }

    /**
     * Get the answer for the questions.
     */
    public function questions()
    {
        return $this->hasOne(Question::class, 'id', 'question_id');
    }
}
