<?php

use App\QuestionType;
use Illuminate\Database\Seeder;

class QuestionTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        QuestionType::create([ 'name' => 'textbox']);
        QuestionType::create([ 'name' => 'textarea']);
        QuestionType::create([ 'name' => 'radio']);
        QuestionType::create([ 'name' => 'checkbox']);
        QuestionType::create([ 'name' => 'rating']);
    }
}
