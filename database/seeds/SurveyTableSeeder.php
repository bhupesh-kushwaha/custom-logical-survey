<?php

use App\Question;
use App\QuestionOption;
use App\Survey;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class SurveyTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Add survey
        $survey = Survey::create([
            'name' => 'Automobile Expo',
            'expiry_date' => Carbon::now()->addDays(30)
        ]);

        // Add question
        $question1 = Question::create([
            'survey_id' => $survey->id,
            'question_type_id' => 1,
            'name' => 'Please enter your price in LAKH?',
            'mulitple' => Question::QUESTION_MULTIPLE_NO
        ]);

        $question2 = Question::create([
            'survey_id' => $survey->id,
            'question_type_id' => 2,
            'name' => 'Please enter your expetion?',
            'mulitple' => Question::QUESTION_MULTIPLE_NO
        ]);

        $question3 = Question::create([
            'survey_id' => $survey->id,
            'question_type_id' => 3,
            'name' => 'Please select your choice?',
            'mulitple' => Question::QUESTION_MULTIPLE_NO
        ]);

        $question4 = Question::create([
            'survey_id' => $survey->id,
            'question_type_id' => 4,
            'name' => 'Please select your choice?',
            'mulitple' => Question::QUESTION_MULTIPLE_YES
        ]);

        $question5 = Question::create([
            'survey_id' => $survey->id,
            'question_type_id' => 5,
            'name' => 'Please rate your self for this?',
            'mulitple' => Question::QUESTION_MULTIPLE_NO
        ]);

        // Add question options
        QuestionOption::create(['question_id' => $question3->id,'name' => 'Car']);
        QuestionOption::create(['question_id' => $question3->id,'name' => 'Bike']);

        QuestionOption::create(['question_id' => $question4->id,'name' => 'Petrol']);
        QuestionOption::create(['question_id' => $question4->id,'name' => 'Diesel']);
        QuestionOption::create(['question_id' => $question4->id,'name' => 'CNG']);
    }
}
