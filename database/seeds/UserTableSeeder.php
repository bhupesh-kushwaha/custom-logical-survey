<?php

use App\User;
use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'Admin User',
            'email' => 'admin@admin.com',
            'password' => bcrypt('1234567890'),
            'role' => 'admin',
            'created_at' => \Carbon\Carbon::now()->toDateTimeString()
        ]);

        User::create([
            'name' => 'Tester User',
            'email' => 'tester1@demo.com',
            'password' => bcrypt('1234567890'),
            'role' => 'user',
            'created_at' => \Carbon\Carbon::now()->toDateTimeString()
        ]);
    }
}
