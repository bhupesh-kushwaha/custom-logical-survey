## Custom Logical Survey- Laravel 5.8 | MySQL

    1) Login, Reg.
    2) Custom Logical Survey
        - add Survey (CRUD) (Survey name, expiry date)
        - add questions to Survey (Question, Question Type, if(Multiple choice then option) if single choice then true answer)
        - question type (Textbox, Text area, radio, checkbox, rating)
        - while adding an option to question select redirection question
        Eg. I have created survey AUTO-Mobile expo
        Please select your choice?
            A. CAR -> Car related Question
            B. Bike -> Bike related Question
    3) Show answer in the admin panel
    4) Create One API using Passport  


### TECHNOLOGY

* **Laravel**: Laravel 5.8 - PHP 7.2 or higher.
* **Twitter Bootstrap**: V4.1.1 use for css styling and js.
* **MySQL**: Database.
* **jQuery/**: Front end framework.

### Installation and Configuration

**1. Enter git clone and the repository URL at your command line::** 
~~~
git clone https://bhupesh19921@bitbucket.org/bhupesh-kushwaha/custom-logical-survey.git
~~~

**2. Goto custom-logical-survey directory and composer update:** 
~~~
composer update
~~~

**3. Copy `env.example` to `.env` and generate app key:** 
~~~
cp .env.example .env

php artisan key:generate
~~~

**3.1. You need to give permision for below folders**
~~~
sudo chmod -R 0777 bootstrap/ storage/ storage/logs/
~~~

**3.2. You need to set your `APP_NAME` and `APP_URL` from `.env` file** 

**4. Create a database `custom_logical_survey_db` or if you want to change database name just go in .env file and change value for `DB_DATABASE` key:**

**5. Now, Run migration to create a table in your database:** 
~~~
php artisan migration
~~~

**5.1. Now, Run passport command to create "personal access" and "password grant" clients keys** 
~~~
php artisan passport:install
~~~

**6. Now, Run seed to store survey and question data to start:** 
~~~
php artisan db:seed
~~~

**7. Finally, Start your server:**
~~~
php artisan serve
~~~
