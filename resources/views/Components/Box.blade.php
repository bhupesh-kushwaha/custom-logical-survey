<div>
    @forelse ($groupedByUser as $key => $lists)
        <h4>{{ $key }}
            <a href="javascript:void(0)" class="float-right">{{$lists[0]['created_at'] }}</a>
        </h4>

        @foreach ($lists as $item)
        <ul class="timeline">
            <li>
                <a href="javascript:void(0)">Que. {{ $item['question'] }}</a>


                <p></p>
                <p><b>Ans. {{ $item['answer'] }}</b></p>
            </li>
        </ul>
        @endforeach
    @empty
        No, Records found
    @endforelse
</div>


<hr/>
