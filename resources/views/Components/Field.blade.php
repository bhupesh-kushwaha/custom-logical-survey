    @php
        $displayNone = "";
        $dataAttribute = "";

        if( $questions->question_id != 0 && $questions->option_id!= 0 ) {
            $displayNone = "";
            $dataAttribute = $questions->question_id."_".$questions->option_id;
        }
    @endphp

    @if($questions->question_type_id == 1)
        <div data-url="{{ $url }}" class="parentSection col-md-12 {{ $displayNone }} {{"multiple-" . $dataAttribute}}"  data-optionid="{{$dataAttribute}}">
            <div class="form-group">
                <label for="input_{{$survey->id . "_" . $questions->question_type_id."_".$questions->question_id."_".$questions->option_id}}">{{ $questions->name }}:</label>
            <input
                type="text"
                class="form-control"
                id="input_{{$survey->id . "_" . $questions->id."_".$questions->question_id."_".$questions->option_id}}"
                name="input_{{$survey->id . "_" . $questions->id}}"
                placeholder="{{ $questions->name }}"
                required />
                <div class="invalid-feedback">
                    {{ $questions->name }}!
                </div>
            </div>
        </div>
        <div class="nestedQuestion col-md-12"></div>

    @elseif($questions->question_type_id == 2)

        <div data-url="{{ $url }}" class="parentSection col-md-12 {{ $displayNone }} {{"multiple-" . $dataAttribute}}"  data-optionid="{{$dataAttribute}}">
            <div class="form-group">
                <label for="textare_{{$survey->id . "_" . $questions->question_type_id."_".$questions->question_id."_".$questions->option_id}}">{{ $questions->name }}:</label>

                <textarea
                class="form-control"
                id="textare_{{$survey->id . "_" . $questions->id."_".$questions->question_id."_".$questions->option_id}}"
                name="textare_{{$survey->id . "_" . $questions->id}}"
                placeholder="{{ $questions->name }}"
                required></textarea>

                <div class="invalid-feedback">
                    {{ $questions->name }}!
                </div>
            </div>
        </div>
        <div class="nestedQuestion col-md-12"></div>


    @elseif($questions->question_type_id == 3)
        <div data-url="{{ $url }}" class="parentSection col-md-12 {{ $displayNone }} {{"multiple-" . $dataAttribute}}"  data-optionid="{{$dataAttribute}}">
            <div class="form-group">
                <label for="option_{{$survey->id . "_" . $questions->question_type_id}}">{{ $questions->name }}:</label>

                @if( $questions->options )

                    @foreach ($questions->options as $option)

                        <div class="form-check">
                            <label class="form-check-label">
                                <input
                                    type="radio"
                                    class="form-check-input {{ $questions->mulitple == 1 ?  'isMultiple' : '' }}"
                                    id="radio_{{ $survey->id . "_" . $questions->id."_".$questions->question_id."_".$questions->option_id ."_" . $option->id }}"
                                    name="radio_{{ $survey->id . "_" . $questions->id }}"
                                    value="{{ $option->id }}"
                                    > {{ $option->name }}
                            </label>
                        </div>

                    @endforeach

                @endif

            </div>
        </div>
        <div class="nestedQuestion col-md-12"></div>

    @elseif($questions->question_type_id == 4)
        <div data-url="{{ $url }}" class="parentSection col-md-12 {{ $displayNone }} {{"multiple-" . $dataAttribute}}"  data-optionid="{{$dataAttribute}}">
            <div class="form-group">
                <label for="option_{{$survey->id . "_" . $questions->question_type_id}}">{{ $questions->name }}:</label>

                @if( $questions->options )

                    @foreach ($questions->options as $option)

                        <div class="form-check">
                            <label class="form-check-label">
                                <input
                                    type="checkbox"
                                    class="form-check-input"
                                    id="checkbox_{{ $survey->id . "_" . $questions->id."_".$questions->question_id."_".$questions->option_id ."_" . $option->id }}"
                                    name="checkbox_{{ $survey->id . "_" . $questions->id }}[]"
                                    value="{{ $option->id }}"
                                    > {{ $option->name }}
                            </label>
                        </div>

                    @endforeach

                @endif

            </div>
        </div>
        <div class="nestedQuestion col-md-12"></div>


    @elseif($questions->question_type_id == 5)
        <div data-url="{{ $url }}" class="parentSection col-md-12 {{ $displayNone }} {{"multiple-" . $dataAttribute}}"  data-optionid="{{$dataAttribute}}">

            <div class="form-group" id="rating-ability-wrapper">
                <label class="control-label" for="rating">
                <span class="field-label-header">How would you rate your ability to use the computer and access internet?*</span><br>
                <span class="field-label-info"></span>

                <input type="hidden"
                    class="selected_rating"
                    id="{{$survey->id . "_" . $questions->id."_".$questions->question_id."_".$questions->option_id}}"
                    name="rating_{{$survey->id . "_" . $questions->question_type_id}}"
                    value="" required="required"
                />


            </label>
                <h2 class="bold rating-header" style="">
                <span class="selected-rating">0</span><small> / 5</small>
                </h2>
                <button type="button" class="btnrating btn btn-default btn-lg rating-star-1" data-attr="1">
                    <i class="fa fa-star" aria-hidden="true"></i>
                </button>
                <button type="button" class="btnrating btn btn-default btn-lg rating-star-2" data-attr="2">
                    <i class="fa fa-star" aria-hidden="true"></i>
                </button>
                <button type="button" class="btnrating btn btn-default btn-lg rating-star-3" data-attr="3">
                    <i class="fa fa-star" aria-hidden="true"></i>
                </button>
                <button type="button" class="btnrating btn btn-default btn-lg rating-star-4" data-attr="4">
                    <i class="fa fa-star" aria-hidden="true"></i>
                </button>
                <button type="button" class="btnrating btn btn-default btn-lg rating-star-5" data-attr="5">
                    <i class="fa fa-star" aria-hidden="true"></i>
                </button>
            </div>

        </div>
        <div class="nestedQuestion col-md-12"></div>
    @else

    @endif
