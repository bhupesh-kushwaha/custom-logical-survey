@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif


                    <div class="row user-left-part">
                        <div class="col-md-12 col-sm-12 col-xs-12 pull-right profile-right-section">
                            <div class="row justify-content-center">
                                <div class="col-md-10">
                                    <h4> Survey Results</h4>

                                    <select id="surveyID" style="width: 100%">
                                        <option>--Select Any one--</option>
                                        @foreach ($surveyList as $key => $value)
                                            <option {{ $selected == $key ? "selected" : "" }} value="{{ $key}}">{{ $value }}</option>
                                        @endforeach
                                    </select>


                                    <div class="auto-scroll mt-5">
                                        @forelse ($surveyViewBox as $item)
                                            {!! $item !!}
                                        @empty
                                            No records found!
                                        @endforelse
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>



                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('styles')
    <style>
        ul.timeline {
    list-style-type: none;
    position: relative;
}

ul.timeline:before {
    content: ' ';
    background: #d4d9df;
    display: inline-block;
    position: absolute;
    left: 29px;
    width: 2px;
    height: 100%;
    z-index: 400;
}

ul.timeline>li {
    margin: 20px 0;
    padding-left: 20px;
}

ul.timeline>li:before {
    content: ' ';
    background: white;
    display: inline-block;
    position: absolute;
    border-radius: 50%;
    border: 3px solid #22c0e8;
    left: 20px;
    width: 20px;
    height: 20px;
    z-index: 400;
}

.auto-scroll {
    height: 300px;
    overflow-y: auto;
}
        </style>
@endpush

@push('scripts')
<script>
    $( document ).ready(function() {
        $('body').on('change', '#surveyID', function() {
            var id = $(this).val();

            if (id) {
                window.location = "{{ url('/') }}/admin?action=result&id="+id;
            }

            return false;
        });
    });
</script>
@endpush
