@extends('layouts.app')

@section('content')
<div class="container">

    <div class="row">
        <div class="col-12">
          <a href="javascript:void(0)" class="btn btn-success mb-2" id="create-new-option">Add option</a>

          <a
            href="{{ route('admin.question.index', ['survey' => $surveyID] ) }}"
            class="btn btn-danger mb-2 float-right" id="create-new-option">Back</a>

          <table class="table table-bordered" id="option_crud">
           <thead>
              <tr>
                 <th>Id</th>
                 <th>Question Name</th>
                 <th>Name</th>
                 <td colspan="2">Action</td>
              </tr>
           </thead>
           <tbody id="options-crud">
              @forelse($options as $option)
                <tr id="option_id_{{ $option->id }}">
                    <td>{{ $option->id  }}</td>
                    <td>{{ $questionName }}</td>
                    <td>{{ $option->name }}</td>
                    <td>
                        <a href="javascript:void(0)" id="edit-option" data-id="{{ $option->id }}" class="btn btn-info">Edit</a>
                    </td>

                    <td>
                        <a href="javascript:void(0)" id="delete-option" data-id="{{ $option->id }}" class="btn btn-danger delete-option">Delete</a>
                    </td>


                    @if ($is_multiple == 1)
                        <td>
                            <a href="{{ route('admin.muliple.question.index' , [
                                'question_id' => $questionId,
                                'option_id' => $option->id,
                            ]) }}" class="btn btn-warning">Add Nested Question</a>
                        </td>
                    @endif
                </tr>
              @empty
                <tr><td colspan="8">No Record found!</td></tr>
              @endforelse
           </tbody>
          </table>
          {{ $options->links() }}
       </div>
    </div>
</div>


<div class="modal fade" id="ajax-crud-modal" aria-hidden="true">
<div class="modal-dialog">
<div class="modal-content">
    <div class="modal-header">
        <h4 class="modal-title" id="optionCrudModal"></h4>
    </div>
    <div class="modal-body">
        <form id="optionForm" name="optionForm" class="form-horizontal">
            <input type="hidden" name="question_id" id="question_id" value="{{ $questionId }}">

           <input type="hidden" name="option_id" id="option_id">

           <div class="form-group">
                <label for="option_name" class="col-md-12 control-label">Title</label>
                <div class="col-sm-12">
                    <input type="text" class="form-control modal-attributes" id="option_name" name="option_name" value="" required="">
                    <span class="text-danger" id="option_name_error"></span>
                </div>
            </div>

            <div class="col-sm-offset-2 col-sm-10">
             <button type="button" class="btn btn-primary" id="btn-save" value="create-option">Save
             </button>
            </div>
        </form>
    </div>
    <div class="modal-footer">

    </div>
</div>
</div>
</div>
@endsection

@push('styles')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css" />
@endpush
@push('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js"></script>

<script>
    $(document).ready(function () {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $('#option_expiry_date').datepicker({
            format: 'yyyy/mm/dd',
        });

        $('body').on('change', '.modal-attributes', function() {
            var currentValue = $(this).val();
            var spanTest = $(this).siblings('span').text;

            if( spanTest.length !== 0 && currentValue.length !== 0 ) {
                $(this).siblings('span').text('');
            }
        });


        $('#create-new-option').click(function () {
            $('#btn-save').val("create-option");
            $('#optionForm').trigger("reset");
            $('#optionCrudModal').html("Add New option");
            $('#ajax-crud-modal').modal('show');
        });

        $('body').on('click', '#edit-option', function () {
            var option_id = $(this).data('id');
            var question_id = $('#question_id').val();
            $.get( "{{url('/admin/option/')}}/" + question_id + '/'+option_id+'/edit', function (data) {

                $('#optionCrudModal').html("Edit option");
                $('#btn-save').val("edit-option");
                $('#ajax-crud-modal').modal('show');

                $('#option_id').val(data.id);
                $('#option_type').val(data.option_type_id);
                $('#option_name').val(data.name);
                $('#option_mulitple').val(data.mulitple);
            })
        });

        $('body').on('click', '.delete-option', function () {
            var option_id = $(this).data("id");
            var question_id = $('#question_id').val();
            if(confirm("Are You sure want to delete !")) {
                $.ajax({
                    type: "DELETE",
                    url: "{{ url('/admin/option')}}"+'/'+option_id+'/delete',
                    success: function (data) {
                        $("#option_id_" + option_id).remove();
                    },
                    error: function (data) {
                        console.log('Error:', data);
                    }
                });
            }
        });

        $('body').on('click', '#btn-save', function(e) {
            e.preventDefault();

            var actionType = $('#btn-save').val();
            $('#btn-save').html('Sending..');
            $.ajax({
                data: $('#optionForm').serialize(),
                url: "{{ route('admin.option.store') }}",
                type: "post",
                dataType: 'json',
                success: function (data) {
                    location.reload();
                },
                error: function (reject) {
                    if( reject.status === 422 ) {
                        var errors = $.parseJSON(reject.responseText);

                        $.each(errors.errors, function (key, val) {
                            $("#" + key + "_error").text(val[0]);
                        });
                    }

                    $('#btn-save').html('Save');
                }
            });
        });
    });
</script>
@endpush
