@extends('layouts.app')

@section('content')
<div class="container">

    <div class="row">
        <div class="col-12">
          <a href="javascript:void(0)" class="btn btn-success mb-2" id="create-new-question">Add question</a>

          <a
            href="{{ route('admin.option.index', [
                'question' => $questionDetails->id,
                'is_multiple' => $questionDetails->mulitple
            ] ) }}"
            class="btn btn-danger mb-2 float-right" id="create-new-option">Back</a>

          <table class="table table-bordered" id="question_crud">
           <thead>
              <tr>
                 <th>Id</th>
                 <th>Parent Question</th>
                 <th>Parent option</th>
                 <th>Question Type</th>
                 <th>Name</th>
                 <th>IS Multiple</th>
                 <td colspan="2">Action</td>
              </tr>
           </thead>
           <tbody id="questions-crud">
              @forelse($questions as $question)
                <tr id="question_id_{{ $question->id }}">
                    <td>{{ $question->id  }}</td>
                    <td>{{ $questionDetails->name }}</td>
                    <td>{{ $optionDetails->name }}</td>
                    <td>{{ $question->question_types->first()->name }}</td>
                    <td>{{ $question->name }}</td>
                    <td>{{ $question->mulitple == 1 ? 'yes' : 'no' }}</td>
                    <td>
                        <a href="javascript:void(0)" id="edit-question" data-id="{{ $question->id }}" class="btn btn-info">Edit</a>
                    </td>

                    <td>
                        <a href="javascript:void(0)" id="delete-question" data-id="{{ $question->id }}" class="btn btn-danger delete-question">Delete</a>
                    </td>

                    @if (in_array($question->question_type_id, [3,4]))
                        <td>
                            <a href="{{ route('admin.option.index' , [
                                'id' => $question->id,
                                'is_multiple' => $question->mulitple,
                            ]) }}" class="btn btn-warning">Add Options</a>
                        </td>
                    @endif

                </tr>
              @empty
                <tr><td colspan="8">No Record found!</td></tr>
              @endforelse
           </tbody>
          </table>
          {{ $questions->links() }}
       </div>
    </div>
</div>


<div class="modal fade" id="ajax-crud-modal" aria-hidden="true">
<div class="modal-dialog">
<div class="modal-content">
    <div class="modal-header">
        <h4 class="modal-title" id="questionCrudModal"></h4>
    </div>
    <div class="modal-body">
        <form id="questionForm" name="questionForm" class="form-horizontal">
            <input type="hidden" name="survey_id" id="survey_id" value="{{ $questionDetails->survey_id }}">
            <input type="hidden" name="parent_question_id" id="parent_question_id" value="{{ $questionDetails->id }}">
            <input type="hidden" name="parent_option_id" id="parent_option_id" value="{{ $optionDetails->id }}">

           <input type="hidden" name="question_id" id="question_id">

           <div class="form-group">
                <label for="question_type" class="col-md-12 control-label">Question Type</label>
                <div class="col-sm-12">
                    <select class="form-control modal-attributes" id="question_type" name="question_type" required="">
                        <option value="">-- Select type --</option>
                        @foreach ($questionType as $key => $value)
                            <option value="{{ $key }}">{{ $value }}</option>
                        @endforeach
                    </select>
                    <span class="text-danger" id="question_type_error"></span>
                </div>
            </div>

           <div class="form-group">
                <label for="question_name" class="col-md-12 control-label">Title</label>
                <div class="col-sm-12">
                    <input type="text" class="form-control modal-attributes" id="question_name" name="question_name" value="" required="">
                    <span class="text-danger" id="question_name_error"></span>
                </div>
            </div>

            <div class="form-group">
                <label for="question_multiple" class="col-md-12 control-label">Question IS Multiple Options</label>
                <div class="col-sm-12">
                    <select class="form-control modal-attributes" id="question_multiple" name="question_multiple" required="">
                        <option value="">-- Select type --</option>
                        <option value="1">Yes</option>
                        <option value="0">No</option>
                    </select>
                    <span class="text-danger" id="question_multiple_error"></span>
                </div>
            </div>

            <div class="col-sm-offset-2 col-sm-10">
             <button type="button" class="btn btn-primary" id="btn-save" value="create-question">Save
             </button>
            </div>
        </form>
    </div>
    <div class="modal-footer">

    </div>
</div>
</div>
</div>
@endsection

@push('styles')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css" />
@endpush
@push('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js"></script>

<script>
    $(document).ready(function () {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $('#question_expiry_date').datepicker({
            format: 'yyyy/mm/dd',
        });

        $('body').on('change', '.modal-attributes', function() {
            var currentValue = $(this).val();
            var spanTest = $(this).siblings('span').text;

            if( spanTest.length !== 0 && currentValue.length !== 0 ) {
                $(this).siblings('span').text('');
            }
        });


        $('#create-new-question').click(function () {
            $('#btn-save').val("create-question");
            $('#questionForm').trigger("reset");
            $('#questionCrudModal').html("Add New question");
            $('#ajax-crud-modal').modal('show');
        });

        $('body').on('click', '#edit-question', function () {
            var question_id = $(this).data('id');
            var survey_id = $('#survey_id').val();
            $.get( "{{url('/')}}/admin/question/" + survey_id + '/'+question_id+'/edit', function (data) {
           // $.get(survey_id + '/'+question_id+'/edit', function (data) {

                $('#questionCrudModal').html("Edit question");
                $('#btn-save').val("edit-question");
                $('#ajax-crud-modal').modal('show');

                $('#question_id').val(data.id);
                $('#question_type').val(data.question_type_id);
                $('#question_name').val(data.name);
                $('#question_mulitple').val(data.mulitple);
            })
        });

        $('body').on('click', '.delete-question', function () {
            var question_id = $(this).data("id");
            var survey_id = $('#survey_id').val();
            if(confirm("Are You sure want to delete !")) {
                $.ajax({
                    type: "DELETE",
                    url: "{{ url('/admin/question')}}"+'/'+question_id+'/delete',
                    success: function (data) {
                        $("#question_id_" + question_id).remove();
                    },
                    error: function (data) {
                        console.log('Error:', data);
                    }
                });
            }
        });

        $('body').on('click', '#btn-save', function(e) {
            e.preventDefault();

            var actionType = $('#btn-save').val();
            $('#btn-save').html('Sending..');
            $.ajax({
                data: $('#questionForm').serialize(),
                url: "{{ route('admin.question.store') }}",
                type: "post",
                dataType: 'json',
                success: function (data) {
                    location.reload();
                },
                error: function (reject) {
                    if( reject.status === 422 ) {
                        var errors = $.parseJSON(reject.responseText);

                        $.each(errors.errors, function (key, val) {
                            $("#" + key + "_error").text(val[0]);
                        });
                    }

                    $('#btn-save').html('Save');
                }
            });
        });
    });
</script>
@endpush
