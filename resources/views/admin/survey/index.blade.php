@extends('layouts.app')

@section('content')
<div class="container">

    <div class="row">
        <div class="col-12">
          <a href="javascript:void(0)" class="btn btn-success mb-2" id="create-new-survey">Add survey</a>

          <table class="table table-bordered" id="survey_crud">
           <thead>
              <tr>
                 <th>Id</th>
                 <th>Name</th>
                 <th>Expiry Daye</th>
                 <td colspan="2">Action</td>
              </tr>
           </thead>
           <tbody id="surveies-crud">
              @foreach($surveies as $survey)
              <tr id="survey_id_{{ $survey->id }}">
                 <td>{{ $survey->id  }}</td>
                 <td>{{ $survey->name }}</td>
                 <td>{{ $survey->expiry_date }}</td>
                 <td><a href="javascript:void(0)" id="edit-survey" data-id="{{ $survey->id }}" class="btn btn-info">Edit</a></td>
                 <td>
                  <a href="javascript:void(0)" id="delete-survey" data-id="{{ $survey->id }}" class="btn btn-danger delete-survey">Delete</a>
                 </td>

                 <td>
                 <a href="{{ route('admin.question.index' , ['id' => $survey->id]) }}" class="btn btn-warning">Add Question</a>
                   </td>
              </tr>
              @endforeach
           </tbody>
          </table>
          {{ $surveies->links() }}
       </div>
    </div>
</div>


<div class="modal fade" id="ajax-crud-modal" aria-hidden="true">
<div class="modal-dialog">
<div class="modal-content">
    <div class="modal-header">
        <h4 class="modal-title" id="surveyCrudModal"></h4>
    </div>
    <div class="modal-body">
        <form id="surveyForm" name="surveyForm" class="form-horizontal">
           <input type="hidden" name="survey_id" id="survey_id">

           <div class="form-group">
                <label for="survey_name" class="col-md-12 control-label">Title</label>
                <div class="col-sm-12">
                    <input type="text" class="form-control modal-attributes" id="survey_name" name="survey_name" value="" required="">
                    <span class="text-danger" id="survey_name_error"></span>
                </div>
            </div>

            <div class="form-group">
                <label for="survey_expiry_date" class="col-md-12 control-label">Expiry Date</label>
                <div class="col-sm-12">
                    <input type="text" class="form-control modal-attributes" id="survey_expiry_date" name="survey_expiry_date" value="" required="">
                    <span class="text-danger" id="survey_expiry_date_error"></span>
                </div>
            </div>

            <div class="col-sm-offset-2 col-sm-10">
             <button type="button" class="btn btn-primary" id="btn-save" value="create-survey">Save
             </button>
            </div>
        </form>
    </div>
    <div class="modal-footer">

    </div>
</div>
</div>
</div>
@endsection

@push('styles')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css" />
@endpush
@push('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js"></script>

<script>
    $(document).ready(function () {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $('#survey_expiry_date').datepicker({
            format: 'yyyy/mm/dd',
        });

        $('body').on('change', '.modal-attributes', function() {
            var currentValue = $(this).val();
            var spanTest = $(this).siblings('span').text;

            if( spanTest.length !== 0 && currentValue.length !== 0 ) {
                $(this).siblings('span').text('');
            }
        });


        $('#create-new-survey').click(function () {
            $('#btn-save').val("create-survey");
            $('#surveyForm').trigger("reset");
            $('#surveyCrudModal').html("Add New survey");
            $('#ajax-crud-modal').modal('show');
        });

        $('body').on('click', '#edit-survey', function () {
            var survey_id = $(this).data('id');
            $.get('survey/'+survey_id+'/edit', function (data) {

                $('#surveyCrudModal').html("Edit survey");
                $('#btn-save').val("edit-survey");
                $('#ajax-crud-modal').modal('show');

                $('#survey_id').val(data.id);
                $('#survey_name').val(data.name);
                $('#survey_expiry_date').val(data.expiry_date);
            })
        });

        $('body').on('click', '.delete-survey', function () {
            var survey_id = $(this).data("id");
            if(confirm("Are You sure want to delete !")) {
                $.ajax({
                    type: "DELETE",
                    url: "{{ url('/admin/survey')}}"+'/'+survey_id+'/delete',
                    success: function (data) {
                        $("#survey_id_" + survey_id).remove();
                    },
                    error: function (data) {
                        console.log('Error:', data);
                    }
                });
            }
        });

        $('body').on('click', '#btn-save', function(e) {
            e.preventDefault();

            var actionType = $('#btn-save').val();
            $('#btn-save').html('Sending..');
            $.ajax({
                data: $('#surveyForm').serialize(),
                url: "{{ route('admin.survey.store') }}",
                type: "post",
                dataType: 'json',
                success: function (data) {
                    location.reload();
                },
                error: function (reject) {
                    if( reject.status === 422 ) {
                        var errors = $.parseJSON(reject.responseText);

                        $.each(errors.errors, function (key, val) {
                            $("#" + key + "_error").text(val[0]);
                        });
                    }

                    $('#btn-save').html('Save');
                }
            });
        });
    });
</script>
@endpush
