@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header"> Logical Survey </div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <select id="surveyID" style="width: 100%">
                        <option>--Select Any one--</option>
                        @foreach ($surveyList as $key => $value)
                            <option value="{{ $key}}">{{ $value }}</option>
                        @endforeach
                    </select>

                    <p class="text-center mt-5">It's just a smalll survey</p>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
<script>
    $( document ).ready(function() {
        $('body').on('change', '#surveyID', function() {
            var id = $(this).val();

            if (id) {
                window.location = "{{ url('/') }}/home/survey/"+id;
            }

            return false;
        });
    });
</script>
@endpush
