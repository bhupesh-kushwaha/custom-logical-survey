@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
            <div class="card-header"> Logical Survey  | {{ $survey->name }}

                <a class="btn btn-sm btn-danger float-right" href="{{ route('home') }}">Back</a>
            </div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif



                    <div class="row">
                        <div class="col-md-12">
                        <form
                            id="bs-validate-demo"
                            method="post"
                            action="{{ route('user.survey.post') }}"
                            class="{{ ($errors->any()) ? 'was-validated': ''}}"
                            enctype="multipart/form-data"
                            novalidate
                        >
                            @csrf

                            <div class="row">
                                @foreach ($surveyRender as $item)
                                    {!! $item !!}
                                @endforeach
                            </div>

                            <button type="submit" class="btn btn-success" name="btnSubmit">Submit Your Suevey</button>

                        </form>
                    </div>




                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('style')
    <style>
        .rating-header {
            margin-top: -10px;
            margin-bottom: 10px;
        }
    </style>
@endpush

@push('scripts')
<link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $( document ).ready(function() {

        $(".btnrating").on('click',(function(e) {

            var previous_value = $("#selected_rating").val();
            var selected_value = $(this).attr("data-attr");

            $('.selected_rating').val(selected_value);

            $(".selected-rating").empty();
            $(".selected-rating").html(selected_value);

            $('.btnrating').addClass('btn-default');
            $('.btnrating').removeClass('btn-warning');

            for (i = 1; i <= selected_value; ++i) {
                $(".rating-star-"+i).removeClass('btn-default');
                $(".rating-star-"+i).addClass('btn-warning');
            }

        }));

        $("body").on('change', '#bs-validate-demo input:radio', function(e){
            e.preventDefault();

            var $this = $(this);

            var id = $this.attr('id');

            var arr = id.split('_');

            $('[class^="multiple-'+arr[2]+'"], [class*=" multiple-'+arr[2]+'"]').addClass('d-none');

            $('.multiple-' + arr[2] + '_' + arr[5]).removeClass('d-none');

            $this.parents().siblings(".nestedQuestion").html('');

            var URL =  $this.closest('.parentSection').data('url');

            $.ajax({
                data: {
                    survey_id: arr[1],
                    quesion_id: arr[2],
                    option_id: arr[5]
                },
                url: URL,
                type: "post",
                dataType: 'json',
                success: function (data) {
                    if( data.html.length > 0 ){
                        $.each( data.html, function( key, value ) {
                            $this.closest('.parentSection').next(".nestedQuestion").append(value);
                        });
                    }
                },
                error: function (reject) {

                }
            });

        });


    });
</script>
@endpush
