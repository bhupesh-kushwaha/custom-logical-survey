<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    //return view('welcome');
    return redirect()->route('home');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/home/survey/{survey}', 'HomeController@getSurvey')->name('home.survey');

Route::post('ajax/questions', 'HomeController@ajaxMultipleQuestions')->name('ajax.questions');

Route::post('/user/servey/post', 'HomeController@saveSurvey')->name('user.survey.post');


Route::group(["namespace"=>"Admin", "prefix" => 'admin'],function() {
    Route::get('/', 'AdminController@index')->name('admin.index');

    Route::get('/survey', 'SurveyController@index')->name('admin.survey.index');
    Route::post('/survey/store', 'SurveyController@store')->name('admin.survey.store');
    Route::get('/survey/{survey}/edit', 'SurveyController@edit')->name('admin.survey.edit');
    Route::delete('/survey/{id}/delete', 'SurveyController@destroy')->name('admin.survey.delete');

    Route::get('/question/{survey}', 'QuestionController@index')->name('admin.question.index');
    Route::post('/question/store', 'QuestionController@store')->name('admin.question.store');
    Route::get('/question/{survey}/{question}/edit', 'QuestionController@edit')->name('admin.question.edit');
    Route::delete('/question/{id}/delete', 'QuestionController@destroy')->name('admin.question.delete');

    Route::get('/option/{question}/mul/{is_multiple}', 'OptionController@index')->name('admin.option.index');
    Route::post('/option/store', 'OptionController@store')->name('admin.option.store');
    Route::get('/option/{question}/{option}/edit', 'OptionController@edit')->name('admin.option.edit');
    Route::delete('/option/{id}/delete', 'OptionController@destroy')->name('admin.option.delete');

    route::get('/question/{question}/option/{option}', 'QuestionController@nestedQuestion')->name('admin.muliple.question.index');
});
